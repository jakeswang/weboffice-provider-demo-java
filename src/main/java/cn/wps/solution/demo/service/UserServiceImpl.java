package cn.wps.solution.demo.service;

import cn.wps.solution.demo.repository.UserRepository;
import cn.wps.solution.weboffice.provider.v3.exception.InvalidArgument;
import cn.wps.solution.weboffice.provider.v3.model.UserInfo;
import cn.wps.solution.weboffice.provider.v3.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public List<UserInfo> fetchUsers(List<String> userIds) {
        return Optional.ofNullable(userIds)
                .map(List::stream)
                .map(s -> s.map(Long::parseLong))
                .map(s -> s.collect(Collectors.toList()))
                .map(s -> userRepository.findAllById(s))
                .map(s -> StreamSupport.stream(s.spliterator(), false))
                .map(s -> s.map(u -> u.toUserInfo()))
                .map(s -> s.collect(Collectors.toList()))
                .orElseThrow(InvalidArgument::new);
    }
}
