package cn.wps.solution.demo.repository;

import cn.wps.solution.demo.entity.File;
import cn.wps.solution.demo.entity.FileId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileRepository extends CrudRepository<File, FileId> {
    List<File> findByIdId(String id);
}
