package cn.wps.solution.demo.entity;

import cn.wps.solution.weboffice.provider.v3.model.FileInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "t_file")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class File {
    @EmbeddedId
    private FileId id;

    private String name;

    private long size;

    private LocalDateTime createTime;

    private LocalDateTime modifyTime;

    @OneToOne
    private User creator;

    @OneToOne
    private User modifier;

    public FileInfo toFileInfo() {
        return FileInfo.builder()
                .id(this.id.getId())
                .name(this.name)
                .version(this.id.getVersion())
                .size(this.size)
                .createTime(this.createTime)
                .modifyTime(this.modifyTime)
                .creatorId(String.valueOf(this.creator.getId()))
                .modifierId(String.valueOf(this.modifier.getId()))
                .build();
    }
}
